# terraform/variables.tf
variable "region" {
  default     = "eu-central-1"
  description = "AWS Region"
}

variable "created_by" {
  default     = "Terraform Local"
  description = "Executer Of CMD"
}

variable "deployment_version" {
  default     = "v0.0.0"
  description = "Version of Service"
}

variable "pipeline_url" {
  default     = "not set"
  description = "Pipeline Url"
}

variable "environment" {
    type = string
    default = "dev"
    description = "Options: dev, staging, production"
}