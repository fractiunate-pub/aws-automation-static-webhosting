# Project Information

This repository contains the project files for my tutorial [_Automate Everything: Deploy your own Website with S3 and Gitlab-Pipelines_](https://fractiunate.hashnode.dev/automate-everything-deploy-your-own-website-with-s3-and-gitlab-pipelines).


## How to run this Pipeline

Parameter | Required | Value/Default 
------ | ------|----------
AWS_ACCESS_KEY_ID      | true     | <AWS_ACCESS_KEY_ID>
AWS_SECRET_ACCESS_KEY  | true   | <AWS_SECRET_ACCESS_KEY>
AWS_DEFAULT_REGION     | false      | eu-central-1





## © Copyright

This project is licensed under MIT.

<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
